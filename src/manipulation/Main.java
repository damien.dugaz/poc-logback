/**
 * 
 */
package manipulation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * POC de LogBack
 *
 * @author Damien Dugaz
 */
public class Main {
	//Le logger est obtenu gr�ve � la LoggerFactory
	final static Logger logger = LoggerFactory.getLogger(Main.class);

	/**
	 * M�thode principale
	 *
	 * @param args : Pas d'args
	 */
	public static void main(final String[] args) {

		//Il existe 5 niveaux d'importance de messages, ici class�s du plus bas au plus haut
		//TRACE < DEBUG < INFO < WARN < ERROR
		//�criture des diff�rents niveaux de log
		logger.trace("Ceci est un message de type TRACE");
		logger.debug("Ceci est un message de type DEBUG");
		logger.info("Ceci est un message de type INFO");
		logger.warn("Ceci est un message de type WARN");
		logger.error("Ceci est un message de type ERROR");

		//Exemple
		final String util = "Dupont";
		//Un param�tre peut �tre affich� � l'aide des accolades {}
		logger.warn("L'utilisateur {} n'a pas pu �tre inscrit !", util);

		//Apr�s ex�cution du main, faire un refresh, puis v�rifier la pr�sence d'un fichier app.log dans resource/logs
		//Le consulter pour v�rifier la bonne �criture des logs
	}
}
